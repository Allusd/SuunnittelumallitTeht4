/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava4observ;

/**
 *
 * @author aleks
 */

public interface Observer {

  public abstract void update(Subject theChangedSubject);

}
