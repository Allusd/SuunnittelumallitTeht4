/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava4observ;

/**
 *
 * @author aleks
 */
public class ClockTimer extends Subject {
    int s;
    int m;
    int h;
    public int getHour(){
        return h;
    }
    public int getMinute(){
        return m;
    }
    public int getSecond(){
        return s;
    }
    void tick(){
        s++;
        if(s>=60){
            m++;
        }else if(m>=60){
            h++;
        }else if(h>=24){
            s=0;
            m=0;
            h=0;
        }
        notify();
    }
    
}
